﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Threading;
using System.IO;
using System.Transactions;
using System.Diagnostics;

namespace SafeSynTools
{
    public partial class Form1 : Form
    {
        string dbType = ConfigurationManager.AppSettings["dbType"].ToString();
        int point_count = Convert.ToInt32(ConfigurationManager.AppSettings["point_count"]);//获取实时同步的筆數
        int point_time = Convert.ToInt32(ConfigurationManager.AppSettings["point_time"]);//获取实时同步的执行间隔时间
        int point_waitX = Convert.ToInt32(ConfigurationManager.AppSettings["point_waitX"]);//获取实时同步的执行间隔倍數
        string moveIdentityid = ConfigurationManager.AppSettings["moveIdentityid"].ToString();
        string reportConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_report"].ToString();//report
        string mainConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_main"].ToString();//main
        string classFuzhuConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_classFuzhu"].ToString();//report
        string classMainConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_classMain"].ToString();//main
        string sql_agentIn = ConfigurationManager.ConnectionStrings["sqlConnectionString_agentIn"].ToString();
        string sql_totalIn = ConfigurationManager.ConnectionStrings["sqlConnectionString_totalIn"].ToString();
        //string sql_share = ConfigurationManager.ConnectionStrings["sqlConnectionString_share"].ToString();
        string errorLogPath = System.Environment.CurrentDirectory + "\\errorlog.txt";//错误日志记录路径
        string successLogPath = System.Environment.CurrentDirectory + "\\successlog.txt";//成功日志记录路径
        System.Timers.Timer timer_clear = new System.Timers.Timer();
        System.Timers.Timer compareDelayTimer = new System.Timers.Timer();
        System.Timers.Timer compareDelayTimerClass = new System.Timers.Timer();
        bool reportDelay = false;
        bool classFuzhuDelay = false;
        bool isFirstDone;
        Thread threadClass;

        public Form1()
        {
            InitializeComponent();
            point_time = point_time * 1000;//秒
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            //创建实时同步的线程
            Thread thread = new Thread(new ThreadStart(syn_dt_user_get_point_record));
            thread.Start();
            timer_clear.AutoReset = false;
            timer_clear.Interval = 10000;
            timer_clear.Elapsed += new System.Timers.ElapsedEventHandler(clear_Elapse);
            timer_clear.Start();

            compareDelayTimer.Interval = 10000;
            compareDelayTimer.AutoReset = false;
            compareDelayTimer.Elapsed += new System.Timers.ElapsedEventHandler(compareSQLTime_Elapsed);
            compareDelayTimer.Start();

            compareDelayTimerClass.Interval = 10000;
            compareDelayTimerClass.AutoReset = false;
            compareDelayTimerClass.Elapsed += new System.Timers.ElapsedEventHandler(compareSQLTimeClass_Elapsed);
            compareDelayTimerClass.Start();

            threadClass = new Thread(new ThreadStart(syn_class));
            threadClass.Start();
        }

        #region 实时同步的方法
        void syn_dt_user_get_point_record()
        {
            string tableString = ConfigurationManager.AppSettings["onlyOne_point"].ToString();
            string[] tablenames = tableString.Split(',');
            int sleep_time = point_time;
            //string date_now = "";
            //string idStr = "";
            //無窮迴圈
            while (true)
            {
                //date_now = System.DateTime.Now.ToString("yyyy-MM-dd");
                //if (dt_users_class_verify_cold() == 0)
                //{
                //    //每日迴圈
                //    while (date_now == System.DateTime.Now.ToString("yyyy-MM-dd"))
                //    {
                        if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour >= 3 && DateTime.Now.Hour < 6)
                        {
                            Thread.Sleep(1000 * 60 * 60 * 3);
                            continue;
                        }
                        //if (dt_users_class_verify() == 0)
                        //{
                        if (isFirstDone)
                        {
                            int count = 0;
                            foreach (var tablename in tablenames)
                            {
                                try
                                {
                                    //List<SqlParameter> LocalSqlParamter = new List<SqlParameter>()
                                    //{
                                    //    new SqlParameter("@tableName","dt_user_get_point_record")
                                    //};
                                    //string querystr = "select lockid from dt_tablelockid where tableName=@tableName";
                                    //var locktable = SqlDbHelper.GetQuery(querystr, LocalSqlParamter.ToArray(), sql_agentIn);
                                    //byte[] lockid = StringConvertByte(locktable.Rows[0]["lockid"].ToString());
                                    //List<string> maxLockid_list = new List<string>();
                                    //List<SqlParameter> AzureSqlParamter = new List<SqlParameter>();
                                    //SqlParameter sqllock = new SqlParameter("@lockid", SqlDbType.Timestamp);
                                    //sqllock.Value = lockid;
                                    //AzureSqlParamter.Add(sqllock);

                                    //string str = "select top " + point_count + " id,identityid,user_id,point_cash,lotterycode,add_time,SourceName,lockid from " + tablename + " where lockid>@lockid and " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "user_id in(select id from dt_users where flog=0) order by lockid asc OPTION (MAXDOP 2)";
                                    //var table = SqlDbHelper.GetQuery(str, AzureSqlParamter.ToArray(), reportConnStr);
                                    string str = "select top " + point_count + " id,identityid,user_id,point_cash,lotterycode,add_time,SourceName from " + tablename + " with(index(index_reportSyn)) where reportSyn=0 and " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "user_id in(select id from dt_users where flog=0) order by add_time";
                                    var table = SqlDbHelper.GetQuery(str, (reportDelay == true ? mainConnStr : reportConnStr));
                                    if (table == null)
                                        continue;
                                    count = table.Rows.Count;
                                    if (table.Rows.Count != 0)//有数据时更新到本地数据库中
                                    {
                                        //idStr = "";
                                        List<SqlParameter> SqlParamter = new List<SqlParameter>();
                                        //for (int i = table.Rows.Count - 1; i >= 0; i--)
                                        //{
                                        //    byte[] a = (byte[])table.Rows[i]["lockid"];
                                        //    string lockid_list = BitConverter.ToString(a).Replace("-", "");
                                        //    maxLockid_list.Add(lockid_list);
                                        //}
                                        //string maxlockid = "0x" + maxLockid_list.Max();
                                        //table.Columns.Remove("lockid");

                                        //for (int r = 0; r < table.Rows.Count; r++)
                                        //{
                                        //    idStr += "@id" + (r + 1) + ",";
                                        //    SqlParamter.Add(new SqlParameter("@id" + (r + 1), table.Rows[r]["id"].ToString().TrimEnd()));
                                        //}
                                        //idStr = idStr.Substring(0, idStr.Length - 1);

                                        WriteLogData("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
                                        TimeSpan tsA1 = new TimeSpan(DateTime.Now.Ticks);
                                        int resultA = SqlDbHelper.RunInsert(table, "isVerify", "dsp_user_get_point_proxySum_self", sql_agentIn);
                                        TimeSpan tsA2 = new TimeSpan(DateTime.Now.Ticks);
                                        TimeSpan tsA = tsA1.Subtract(tsA2).Duration();
                                        string dateDiffA = tsA.Seconds.ToString() + "秒";
                                        FillMsg1("Agent 成功同步汇总" + count + "条数据,耗时:" + dateDiffA);
                                        WriteLogData("Agent 成功同步汇总" + count + "条数据,耗时:" + dateDiffA + "     同步汇总时间:" + DateTime.Now.ToString());
                                        //一樣的資料送到TotalIn
                                        TimeSpan tsT1 = new TimeSpan(DateTime.Now.Ticks);
                                        int resultT = SqlDbHelper.RunInsert(table, "isVerify", "dsp_user_get_point_synSum", sql_totalIn);
                                        TimeSpan tsT2 = new TimeSpan(DateTime.Now.Ticks);
                                        TimeSpan tsT = tsT1.Subtract(tsT2).Duration();
                                        string dateDiffT = tsT.Seconds.ToString() + "秒";
                                        FillMsg1("Total 成功同步汇总" + count + "条数据,耗时:" + dateDiffT);
                                        WriteLogData("Total 成功同步汇总" + count + "条数据,耗时:" + dateDiffT + "    同步汇总时间:" + DateTime.Now.ToString());

                                        table.Columns.Remove("identityid");
                                        table.Columns.Remove("user_id");
                                        table.Columns.Remove("point_cash");
                                        table.Columns.Remove("lotterycode");
                                        table.Columns.Remove("add_time");
                                        table.Columns.Remove("SourceName");
                                        SqlParameter Parameter_id = new SqlParameter("@idTable", SqlDbType.Structured);
                                        Parameter_id.Value = table;
                                        Parameter_id.TypeName = "report_id_type";
                                        SqlParamter.Add(Parameter_id);

                                        if (resultA == 0 && resultT == 0)
                                        {
                                            WriteLogData("主表 開始更新" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.fffffff"));
                                            TimeSpan tsU1 = new TimeSpan(DateTime.Now.Ticks);
                                            SqlDbHelper.ExecuteNonQuery("update " + tablename + " set reportSyn=2 from dt_user_get_point_record with(index(index_id)) where id in (select id from @idTable)", SqlParamter.ToArray(), mainConnStr);
                                            TimeSpan tsU2 = new TimeSpan(DateTime.Now.Ticks);
                                            TimeSpan tsU = tsU1.Subtract(tsU2).Duration();
                                            string dateDiffU = tsU.Seconds.ToString() + "秒";
                                            WriteLogData("主表 結束更新" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.fffffff"));
                                            FillMsg1("成功更新主表" + count + "条数据,耗时:" + dateDiffU);
                                            WriteLogData(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    if (ex.Message != "未将对象引用设置到对象的实例。")
                                    {
                                        FillErrorMsg(tablename + ":" + ex);
                                        WriteErrorLog(tablename + ":" + DateTime.Now.ToString(), ex.ToString());
                                    }
                                }
                                if (count == point_count)
                                {
                                    sleep_time = 1000;
                                }
                                else if (count >= point_count / 3 * 2)
                                {
                                    sleep_time = point_time;
                                }
                                else if (count < point_count / 3 * 2 && count >= point_count / 3)
                                {
                                    sleep_time = point_time * point_waitX;
                                }
                                else if (count < point_count / 3)
                                {
                                    sleep_time = point_time * (point_waitX + 2);
                                }
                                Thread.Sleep(sleep_time);//睡眠时间
                            }
                        }
                        else
                        {
                            Thread.Sleep(5000);
                        }
                        //}
                //    }
                //}
            }
        }

        void syn_class()
        {
            while (true)
            {
                string tablename = "dt_users_class";
                if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour >= 3 && DateTime.Now.Hour < 6)
                {
                    Thread.Sleep(1000 * 60 * 60 * 3);
                    continue;
                }
                try
                {
                    TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks);
                    string str = "select userid from dt_userid";
                    var classtable = SqlDbHelper.GetQuery(str, sql_agentIn);

                    List<SqlParameter> AzureSqlParamter = new List<SqlParameter>();
                    SqlParameter pa_userid = new SqlParameter("@userid", SqlDbType.Int);
                    pa_userid.Value = classtable.Rows[0]["userid"];
                    AzureSqlParamter.Add(pa_userid);
                    AzureSqlParamter.Add(new SqlParameter("@dbtype", dbType));

                    str = "select id,identityid,father_id,user_id,user_class,isagent,uclass,addtime from dt_users_class where user_id>@userid and identityid in (select identityid from dt_tenant where dbtype=@dbtype) order by user_id";
                    var table = SqlDbHelper.GetQuery(str, AzureSqlParamter.ToArray(), (classFuzhuDelay == true ? classMainConnStr : classFuzhuConnStr));
                    //var table = SqlDbHelper.GetQuery(str, AzureSqlParamter.ToArray(), (reportDelay == true ? mainConnStr : reportConnStr));
                    if (table.Rows.Count > 0)
                    {
                        string idStr = "";
                        //檢查本地有沒有一樣的id
                        for (int r = 0; r < table.Rows.Count; r++)
                        {
                            idStr += table.Rows[r]["id"].ToString().TrimEnd() + ",";
                        }
                        idStr = idStr.Substring(0, idStr.Length - 1);
                        str = string.Format("delete from {0} where id in (" + idStr + ")", tablename);
                        SqlDbHelper.ExecuteNonQuery(str, sql_agentIn);

                        //执行批量插入操作
                        bool successShare = false;
                        RunSqlBulkCopy(table, tablename, ref successShare, sql_agentIn);
                        string sql = string.Format("update dt_userid set userid='{0}',add_time='{1}' ", table.Rows[table.Rows.Count - 1]["user_id"], DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        if (successShare == true)
                            SqlDbHelper.ExecuteNonQuery(sql, sql_agentIn);
                    }
                    TimeSpan ts2 = new TimeSpan(DateTime.Now.Ticks);
                    TimeSpan ts = ts1.Subtract(ts2).Duration();
                    string dateDiff = ts.Seconds.ToString() + "秒";
                    FillMsg2("dt_user_class成功同步汇总" + table.Rows.Count + "条数据,耗时:" + dateDiff);
                    if (!isFirstDone)
                        isFirstDone = true;

                }
                catch (Exception ex)
                {
                    if (ex.Message != "未将对象引用设置到对象的实例。")
                    {
                        FillErrorMsg("dt_users_class:" + ex);
                        WriteErrorLog("dt_users_class:" + DateTime.Now.ToString(), ex.ToString() + "\r\n");
                    }
                }
                Thread.Sleep(5000);
            }
        }

        /// <summary>
        /// 代理汇总前先判断下层级表的准确性 昨天前
        /// </summary>
        int dt_users_class_verify_cold()
        {
            int result = 0;
            int count = 0;
            int reportcount = 0;
            int azurecount = 0;
            int reportmax = 0;
            int azuremax = 0;
            try
            {
                string date_now = System.DateTime.Now.ToString("yyyy-MM-dd");
                //比對每日的數量及最大最小值
                List<SqlParameter> LocalSqlParamter = new List<SqlParameter>()
                {
                    new SqlParameter("@date_now",date_now)
                };
                string str = "select convert(char(10),addtime,121) addtime,count(id) count,isnull(min(id),0) min,isnull(max(id),0) max from dt_users_class where " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "addtime < @date_now group by convert(char(10),addtime,121) order by convert(char(10),addtime,121)";
                //获取报表中的层级最新的id
                var reporttable = SqlDbHelper.GetQuery(str, LocalSqlParamter.ToArray(), sql_agentIn);
                //获取从库中层级最新的id
                var azurettable = SqlDbHelper.GetQuery(str, LocalSqlParamter.ToArray(), (DateTime.Now.Hour >= 0 && DateTime.Now.Hour < 9 && reportDelay == true ? mainConnStr : reportConnStr));
                //var azurettable = SqlDbHelper.GetQuery(str, LocalSqlParamter.ToArray(), reportConnStr);
                TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks);
                for (int i = 0; i < azurettable.Rows.Count; i++)
                {
                    string date_str = azurettable.Rows[i]["addtime"].ToString().TrimEnd();
                    string date_str_e = Convert.ToDateTime(azurettable.Rows[i]["addtime"]).AddDays(1).ToString("yyyy-MM-dd");
                    azurecount = Convert.ToInt32(azurettable.Rows[i]["count"]);
                    azuremax = Convert.ToInt32(azurettable.Rows[i]["max"]);

                    if (reporttable.Select("addtime ='" + date_str + "'").Count() > 0)
                    {
                        reportmax = Convert.ToInt32(reporttable.Select("addtime ='" + date_str + "'")[0]["max"]);
                        reportcount = Convert.ToInt32(reporttable.Select("addtime ='" + date_str + "'")[0]["count"]);
                        if (azurecount > reportcount || azuremax > reportmax)//如報表有叢庫當天資料,並且報表數量和maxid都小於叢庫
                        {
                            count = Crue_data(azurecount, reportcount, azuremax, reportmax, date_str, date_str_e);
                        }
                    }
                    else //如果報表沒叢庫當天數據則補數據
                    {
                        count = Crue_data(azurecount, reportcount, azuremax, reportmax, date_str, date_str_e);
                    }
                }
                TimeSpan ts2 = new TimeSpan(DateTime.Now.Ticks);
                TimeSpan ts = ts1.Subtract(ts2).Duration();
                string dateDiff = ts.Seconds.ToString() + "秒";
                FillMsg2("dt_user_class成功同步汇总" + count + "条数据,耗时:" + dateDiff);
                return result;
            }
            catch (Exception ex)
            {
                FillErrorMsg("dt_users_class_verify_cold:" + ex);
                WriteErrorLog("dt_users_class_verify_cold:" + DateTime.Now.ToString(), ex.ToString() + "  補冷數據");
                return 1;
            }
        }
        /// <summary>
        /// 代理汇总前先判断下层级表的准确性  今天
        /// </summary>
        int dt_users_class_verify()
        {
            int result = 0;
            try
            {
                int count = 0;
                int reportcount = 0;
                int azurecount = 0;
                int reportmax = 0;
                int azuremax = 0;
                int reportmin = 0;
                int azuremin = 0;
                string date_now = System.DateTime.Now.ToString("yyyy-MM-dd");
                List<SqlParameter> LocalSqlParamter = new List<SqlParameter>();
                SqlParameter patime = new SqlParameter("@addtime", SqlDbType.DateTime);
                patime.Value = date_now;
                LocalSqlParamter.Add(patime);

                //int xx = Convert.ToInt32("sss0d5fg4");
                string str = "select count(id) count,isnull(min(id),0) min,isnull(max(id),0) max  from dt_users_class where " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "addtime >= @addtime";
                //获取报表中的层级最新的id
                var reporttable = SqlDbHelper.GetQuery(str, LocalSqlParamter.ToArray(), sql_agentIn);
                if (reporttable.Rows.Count > 0 && reporttable != null)
                {
                    reportcount = Convert.ToInt32(reporttable.Rows[0]["count"]);
                    reportmax = Convert.ToInt32(reporttable.Rows[0]["max"]);
                    reportmin = Convert.ToInt32(reporttable.Rows[0]["min"]);
                    //获取从库中层级最新的id
                    var azurettable = SqlDbHelper.GetQuery(str, LocalSqlParamter.ToArray(), (DateTime.Now.Hour >= 0 && DateTime.Now.Hour < 9 && reportDelay == true ? mainConnStr : reportConnStr));
                    //var azurettable = SqlDbHelper.GetQuery(str, LocalSqlParamter.ToArray(), reportConnStr);
                    azurecount = Convert.ToInt32(azurettable.Rows[0]["count"]);
                    azuremax = Convert.ToInt32(azurettable.Rows[0]["max"]);
                    azuremin = Convert.ToInt32(azurettable.Rows[0]["min"]);
                    if (reportmax >= azuremax && reportcount >= azurecount)//叢庫id<=報表id且叢庫數量<=報表數量表示同步數量相同
                    {
                        return result;
                    }
                    else if (reportcount < azurecount || reportmax < azuremax)//如報表有叢庫當天資料,並且報表數量和maxid都小於叢庫
                    {

                        string date_str = System.DateTime.Now.ToString("yyyy-MM-dd");
                        string date_str_e = System.DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                        //查看叢庫中id等於報表最大id內的資料比數
                        List<SqlParameter> UserClassSqlParamter = new List<SqlParameter>();
                        SqlParameter patime_now = new SqlParameter("@date_now", SqlDbType.DateTime);
                        patime_now.Value = date_now;
                        SqlParameter pamax = new SqlParameter("@reportmax", SqlDbType.Int);
                        pamax.Value = reportmax;
                        UserClassSqlParamter.Add(patime_now);
                        UserClassSqlParamter.Add(pamax);
                        string report_count_str = "select count(id) count  from dt_users_class where id<= @reportmax and " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "addtime>=@date_now";
                        var check_report = SqlDbHelper.GetQuery(report_count_str, UserClassSqlParamter.ToArray(), sql_agentIn);
                        int check_reportcount = Convert.ToInt32(check_report.Rows[0]["count"]);
                        var check_azuret = SqlDbHelper.GetQuery(report_count_str, UserClassSqlParamter.ToArray(), (DateTime.Now.Hour >= 0 && DateTime.Now.Hour < 9 && reportDelay == true ? mainConnStr : reportConnStr));
                        //var check_azuret = SqlDbHelper.GetQuery(report_count_str, UserClassSqlParamter.ToArray(), reportConnStr);
                        int check_azurecount = Convert.ToInt32(check_azuret.Rows[0]["count"]);
                        // string crue_str = "insert into dt_users_class (id,identityid,father_id,user_id,user_class,isagent,uclass,addtime)values";
                        bool issuccess = false;
                        string str_azure = "";
                        try
                        {
                            //string str_report = "";
                            TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks);
                            if (check_reportcount >= check_azurecount)//報表比數大於等於叢庫,表示只需要同步新資料
                            {
                                List<SqlParameter> UserClass_SqlParamter = new List<SqlParameter>();
                                SqlParameter padate_s = new SqlParameter("@date_str", SqlDbType.DateTime);
                                padate_s.Value = date_str;
                                UserClass_SqlParamter.Add(padate_s);
                                SqlParameter padate_e = new SqlParameter("@date_str_e", SqlDbType.DateTime);
                                padate_e.Value = date_str_e;
                                UserClass_SqlParamter.Add(padate_e);
                                UserClass_SqlParamter.Add(pamax);

                                str_azure = "select id,identityid,father_id,user_id,user_class,isagent,uclass,convert(varchar,addtime,121) as addtime from dt_users_class where id>@reportmax and " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "addtime >=@date_str and addtime<=@date_str_e";
                                DataTable dt_azure = SqlDbHelper.GetQuery(str_azure, UserClass_SqlParamter.ToArray(), (DateTime.Now.Hour >= 0 && DateTime.Now.Hour < 9 && reportDelay == true ? mainConnStr : reportConnStr));
                                //DataTable dt_azure = SqlDbHelper.GetQuery(str_azure, UserClass_SqlParamter.ToArray(), reportConnStr);
                                RunSqlBulkCopy(dt_azure, "dt_users_class", ref issuccess, sql_agentIn);
                                count = dt_azure.Rows.Count;
                            }
                            else//需要整天重新檢查
                            {
                                count = Crue_data(azurecount, reportcount, azuremax, reportmax, date_str, date_str_e);
                            }
                            TimeSpan ts2 = new TimeSpan(DateTime.Now.Ticks);
                            TimeSpan ts = ts1.Subtract(ts2).Duration();
                            string dateDiff = ts.Seconds.ToString() + "秒";
                            FillMsg2("dt_user_class成功同步汇总" + count + "条数据,耗时:" + dateDiff);
                        }
                        catch (Exception ex)
                        {
                            FillErrorMsg("dt_users_class_verify:" + ex);
                            FillErrorMsg("sql语句:" + str_azure);
                            WriteErrorLog("dt_users_class_verify:" + DateTime.Now.ToString(), ex.ToString() + "\r\n" + "sql语句:" + str_azure + "\r\n" + "變數check_reportcount:" + check_reportcount + "\t" + "check_azurecount:" + check_azurecount);
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                if (ex.Message != "未将对象引用设置到对象的实例。")
                {
                    FillErrorMsg("dt_users_class_verify:" + ex);
                    WriteErrorLog("dt_users_class_verify: 連線失敗  " + DateTime.Now.ToString(), ex.ToString());
                }
                return 1;
            }
        }
        /// <summary>
        /// 批量插入
        /// </summary>
        private void RunSqlBulkCopy(DataTable dt, string tablename, ref bool isSuccess, string connectString)
        {
            if (dt.Rows.Count != 0)
            {
                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connectString))
                {
                    sqlBulkCopy.DestinationTableName = tablename;
                    sqlBulkCopy.BulkCopyTimeout = 6000;
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (dt.Columns[i].ColumnName != "lockid")
                            sqlBulkCopy.ColumnMappings.Add(dt.Columns[i].ColumnName, dt.Columns[i].ColumnName);
                    }
                    try
                    {
                        isSuccess = true;
                        sqlBulkCopy.WriteToServer(dt);
                    }
                    catch (Exception ex)
                    {
                        isSuccess = false;
                        FillErrorMsg("批量插入" + tablename + ":" + ex);
                        WriteErrorLog("批量插入" + tablename + ":" + DateTime.Now.ToString(), ex.ToString());
                    }
                }
            }
        }
        /// <summary>
        /// 補數據
        /// </summary>
        private int Crue_data(int azurecount, int reportcount, int azuremax, int reportmax, string date_s_str, string date_e_str)
        {
            //誤差比數
            int diff = azurecount - reportcount;
            int crue = 0;
            while (crue < diff || reportmax < azuremax)
            {
                //string date_s_str = date_s.ToString("yyyy-MM-dd");
                //string date_e_str = date_e.ToString("yyyy-MM-dd");
                List<SqlParameter> UserClass_SqlParamter = new List<SqlParameter>();
                SqlParameter padate_s = new SqlParameter("@date_s_str", SqlDbType.DateTime);
                padate_s.Value = date_s_str;
                UserClass_SqlParamter.Add(padate_s);
                SqlParameter padate_e = new SqlParameter("@date_e_str", SqlDbType.DateTime);
                padate_e.Value = date_e_str;
                UserClass_SqlParamter.Add(padate_e);
                string crue_str = "insert into dt_users_class (id,identityid,father_id,user_id,user_class,isagent,uclass,addtime)values";
                string str_azure = "select id,identityid,father_id,user_id,user_class,isagent,uclass,convert(varchar,addtime,121) as addtime from dt_users_class where " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "addtime >=@date_s_str and addtime<=@date_e_str";
                string str_report = "select  id,identityid,father_id,user_id,user_class,isagent,uclass,convert(varchar,addtime,121) as addtime from dt_users_class where " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "addtime >=@date_s_str and addtime<=@date_e_str";
                DataTable dt_azure = SqlDbHelper.GetQuery(str_azure, UserClass_SqlParamter.ToArray(), (DateTime.Now.Hour >= 0 && DateTime.Now.Hour < 9 && reportDelay == true ? mainConnStr : reportConnStr));
                //DataTable dt_azure = SqlDbHelper.GetQuery(str_azure, UserClass_SqlParamter.ToArray(), reportConnStr);
                DataTable dt_report = SqlDbHelper.GetQuery(str_report, UserClass_SqlParamter.ToArray(), sql_agentIn);
                try
                {
                    if (dt_azure.Rows.Count > dt_report.Rows.Count)
                    {
                        for (int r = 0; r < dt_azure.Rows.Count; r++)
                        {
                            string azure_id = dt_azure.Rows[r]["id"].ToString();
                            if (dt_report.Select("id = " + azure_id + "").Count() == 0)
                            {
                                crue++;
                                if (reportmax < Convert.ToInt32(azure_id))
                                {
                                    reportmax = Convert.ToInt32(azure_id);
                                }
                                string identityid = dt_azure.Rows[r]["identityid"].ToString();
                                string father_id = dt_azure.Rows[r]["father_id"].ToString();
                                string user_id = dt_azure.Rows[r]["user_id"].ToString();
                                string user_class = dt_azure.Rows[r]["user_class"].ToString();
                                string isagent = dt_azure.Rows[r]["isagent"].ToString();
                                string uclass = dt_azure.Rows[r]["uclass"].ToString();
                                string addtime = dt_azure.Rows[r]["addtime"].ToString();
                                crue_str += " ('" + azure_id + "','" + identityid + "','" + father_id + "','" + user_id + "','" + user_class + "','" + isagent + "','" + uclass + "','" + addtime + "' ),";
                            }
                        }
                        if (crue > 0)
                        {
                            crue_str = crue_str.Substring(0, crue_str.Length - 1);
                            SqlDbHelper.ExecuteNonQuery(crue_str, sql_agentIn);
                        }
                    }
                    //  date_s = date_s.AddDays(-1);
                    //  date_e = date_e.AddDays(-1);
                }
                catch (Exception ex)
                {
                    FillErrorMsg("Crue_data:" + ex);
                    FillErrorMsg("sql语句:" + crue_str);
                    WriteErrorLog("Crue_data:" + DateTime.Now.ToString(), ex.ToString() + "\r\n" + "sql语句:" + crue_str + "\r\n" + "變數:crue" + crue + "\t" + "azurecount:" + azurecount + "\t" + "reportcount:" + reportcount + "\t" + "azuremax:" + azuremax + "\t" + "reportmax:" + reportmax + "\t" + "date_s_str:" + date_s_str + "\t" + "date_e_str:" + date_e_str);
                }
            }
            return crue;
        }

        private byte[] StringConvertByte(string sqlstring)
        {
            string stringFromSQL = sqlstring;
            List<byte> byteList = new List<byte>();

            string hexPart = stringFromSQL.Substring(2);
            for (int i = 0; i < hexPart.Length / 2; i++)
            {
                string hexNumber = hexPart.Substring(i * 2, 2);
                byteList.Add((byte)Convert.ToInt32(hexNumber, 16));
            }
            byte[] original = byteList.ToArray();
            return original;
        }
        #endregion

        #region richTextBox记录
        private delegate void RichBox1(string msg);
        private void FillMsg1(string msg) 
        {
            if (richTextBox1.InvokeRequired)
            {
                RichBox1 rb = new RichBox1(FillMsg1);
                richTextBox1.Invoke(rb, new object[] { msg });
            }
            else 
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.AppendText(msg);
                    richTextBox1.AppendText("\r\n");
                    richTextBox1.SelectionStart = richTextBox1.Text.Length;
                    richTextBox1.SelectionLength = 0;
                    richTextBox1.Focus();
                }
            }
        }

        private delegate void RichBox2(string msg);
        private void FillMsg2(string msg)
        {
            if (richTextBox2.InvokeRequired)
            {
                RichBox2 rb = new RichBox2(FillMsg2);
                richTextBox2.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox2.IsHandleCreated)
                {
                    richTextBox2.AppendText(msg);
                    richTextBox2.AppendText("\r\n");
                    richTextBox2.SelectionStart = richTextBox2.Text.Length;
                    richTextBox2.SelectionLength = 0;
                    richTextBox2.Focus();
                }
            }
        }

        private delegate void RichBoxErr(string msg);
        private void FillErrorMsg(string msg)
        {
            if (errorBox.InvokeRequired)
            {
                RichBoxErr rb = new RichBoxErr(FillErrorMsg);
                errorBox.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (errorBox.IsHandleCreated)
                {
                    errorBox.AppendText(msg);
                    errorBox.AppendText("\r\n");
                    errorBox.SelectionStart = errorBox.Text.Length;
                    errorBox.SelectionLength = 0;
                    errorBox.Focus();
                }
            }
        }
        #endregion

        #region 打印成功日志记录
        private object obj1 = new object();
        public void WriteLogData(string msgex)
        {
            lock (obj1)
            {
                if (!File.Exists(successLogPath))
                {
                    FileStream fs1 = new FileStream(successLogPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.Write(msgex);
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(successLogPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.Write(msgex);
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion
        
        #region 打印错误日志记录
        private object obj = new object();
        public void WriteErrorLog(string msgex, string msgsql)
        {
            lock (obj)
            {
                if (!File.Exists(errorLogPath))
                {
                    FileStream fs1 = new FileStream(errorLogPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.WriteLine(msgex);
                    sw.WriteLine(msgsql);
                    sw.WriteLine();
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(errorLogPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.WriteLine(msgex);
                    sr.WriteLine(msgsql);
                    sr.WriteLine();
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion

        #region 清理textbox
        private void clear_Elapse(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (System.DateTime.Now.ToString("mm") == "20")
                    ClearMsg();
            }
            catch (ThreadAbortException ex) { }
            catch (Exception ex2)
            {
            }
            finally
            {
                timer_clear.Start();
            }
        }
        private delegate void RichBoxClear();
        private void ClearMsg()
        {
            if (richTextBox1.InvokeRequired & richTextBox2.InvokeRequired & errorBox.InvokeRequired)
            {
                RichBoxClear rb = new RichBoxClear(ClearMsg);
                richTextBox1.Invoke(rb);
                richTextBox2.Invoke(rb);
                errorBox.Invoke(rb);
            }
            else
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.Clear();
                    richTextBox2.Clear();
                    errorBox.Clear();
                }
            }
        }
        #endregion

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Dispose();
            Application.Exit();
            System.Environment.Exit(0);
        }

        private void compareSQLTime_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                string cmd = "select MAX(add_time) as add_time from dt_lottery_orders";
                DataTable tempTable = new DataTable();
                DateTime timeMain;
                DateTime timeReport;

                tempTable = SqlDbHelper.GetQuery(cmd, mainConnStr);
                timeMain = Convert.ToDateTime(tempTable.Rows[0]["add_time"]);
                tempTable = SqlDbHelper.GetQuery(cmd, reportConnStr);
                timeReport = Convert.ToDateTime(tempTable.Rows[0]["add_time"]);

                if (timeMain.Subtract(timeReport).TotalSeconds >= 60)
                {
                    //delaySecond = Convert.ToInt32(timeMain.Subtract(timeReport).TotalSeconds);
                    if (reportDelay == false)
                    {
                        reportDelay = true;
                        WriteErrorLog("--------------------------\r\n報表從庫延遲", DateTime.Now.ToString() + "\r\n--------------------------");
                    }
                }
                else
                {
                    //delaySecond = 0;
                    if (reportDelay == true)
                    {
                        WriteErrorLog("--------------------------\r\n報表從庫恢復", DateTime.Now.ToString() + "\r\n--------------------------");
                    }
                    reportDelay = false;
                }
            }
            catch (Exception ex)
            {
                reportDelay = true;
            }
            finally
            {
                compareDelayTimer.Start();
            }
        }

        private void compareSQLTimeClass_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                string cmd = "select MAX(add_time) as add_time from dt_lottery_orders";
                DataTable tempTable = new DataTable();
                DateTime timeMain;
                DateTime timeReport;

                tempTable = SqlDbHelper.GetQuery(cmd, classMainConnStr);
                timeMain = Convert.ToDateTime(tempTable.Rows[0]["add_time"]);
                tempTable = SqlDbHelper.GetQuery(cmd, classFuzhuConnStr);
                timeReport = Convert.ToDateTime(tempTable.Rows[0]["add_time"]);

                if (timeMain.Subtract(timeReport).TotalSeconds >= 10)
                {
                    //delaySecond = Convert.ToInt32(timeMain.Subtract(timeReport).TotalSeconds);
                    if (classFuzhuDelay == false)
                    {
                        classFuzhuDelay = true;
                        WriteErrorLog("--------------------------\r\n從庫延遲", DateTime.Now.ToString() + "\r\n--------------------------");
                    }
                }
                else
                {
                    //delaySecond = 0;
                    if (classFuzhuDelay == true)
                    {
                        WriteErrorLog("--------------------------\r\n從庫恢復", DateTime.Now.ToString() + "\r\n--------------------------");
                    }
                    classFuzhuDelay = false;
                }
            }
            catch (Exception ex)
            {
                classFuzhuDelay = true;
            }
            finally
            {
                compareDelayTimer.Start();
            }
        }
    }
}
